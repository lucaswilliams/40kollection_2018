﻿using System.ComponentModel.DataAnnotations;

namespace _40Kollection.Models.Database
{
    public class Army
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// The name of your army.  Can be basically anything
        /// For 40K, this could be something like "CSM - Black Legion"
        /// For AoS, this could be something like "Slaves to Darkness"
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The army's faction.  Can be as generic or as broad as you like.
        /// For 40K, this might be broad like "Chaos", or specific like "Chaos Space Marines"
        /// For AoS, this might be broad like "Chaos", or specific like "Khorne Daemons"
        /// </summary>
        public string Faction { get; set; }
    }
}
